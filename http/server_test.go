package http

import (
	"bytes"
	"io/ioutil"
	"local/seabattle"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestServer(t *testing.T) {
	sb := seabattle.NewServer()

	var (
		ts   *httptest.Server
		res  *http.Response
		err  error
		body []byte
	)

	data := []struct {
		name, method string
		cmd          uint8
		data         string
		result       string
	}{
		{
			name:   "CreateMatrix",
			method: http.MethodPost,
			cmd:    seabattle.CmdCreateMatrix,
			data:   `{"range": 5}`,
		},
		{
			name:   "Ship",
			method: http.MethodPost,
			cmd:    seabattle.CmdShip,
			data:   `{"Coordinates": " 2B 4C, 3D 5E"}`,
		},
		{
			name:   "Shot",
			method: http.MethodPost,
			cmd:    seabattle.CmdShot,
			data:   `{"coord": " 3C  "}`,
			result: `{"destroy":false,"knock":true,"end":false}`,
		},
		{
			name:   "Clear",
			method: http.MethodPost,
			cmd:    seabattle.CmdClear,
		},
		{
			name:   "State",
			method: http.MethodGet,
			cmd:    seabattle.CmdState,
			result: `{"ship_count":0,"destroyed":0,"knocked":0,"shot_count":0}`,
		},
	}

	for i, d := range data {
		var dataBad string
		switch i % 2 {
		case 0:
			dataBad = "{}"
		case 1:
			dataBad = ""
		default:
			dataBad = "xyz"
		}

		t.Run(d.name, func(t *testing.T) {
			ts = httptest.NewServer(NewHandler(sb, d.method, d.cmd))
			switch d.method {
			case http.MethodPost:
				res, err = http.Post(ts.URL, contentTypeJson, bytes.NewBufferString(d.data))
				if err != nil {
					t.Fatal(err)
				}
				if res.StatusCode != http.StatusOK {
					t.Fatal(res.StatusCode)
				}

				body, _ = ioutil.ReadAll(res.Body)
				res.Body.Close()

				if string(body) != d.result {
					t.Errorf("%s != %s", body, d.result)
				}

				if d.cmd != seabattle.CmdClear {
					res, err = http.Post(ts.URL, contentTypeJson, bytes.NewBufferString(dataBad))
					if err != nil {
						t.Fatal(err)
					}
					if res.StatusCode != http.StatusBadRequest {
						t.Fatal(res.StatusCode)
					}
				}

				res, err = http.Get(ts.URL)
				if err != nil {
					t.Fatal(err)
				}
				if res.StatusCode != http.StatusNotFound {
					t.Fatal(res.StatusCode)
				}

				ts.Close()

			case http.MethodGet:
				res, err = http.Get(ts.URL)
				if err != nil {
					t.Fatal(err)
				}
				if res.StatusCode != http.StatusOK {
					t.Fatal(res.StatusCode)
				}

				body, _ = ioutil.ReadAll(res.Body)
				res.Body.Close()

				if string(body) != d.result {
					t.Errorf("%s != %s", body, d.result)
				}

				res, err = http.Post(ts.URL, contentTypeJson, bytes.NewBufferString(d.data))
				if err != nil {
					t.Fatal(err)
				}
				if res.StatusCode != http.StatusNotFound {
					t.Fatal(res.StatusCode)
				}

				ts.Close()
			}
		})
	}

}
