package http

import (
	"io/ioutil"
	"local/seabattle"
	"net/http"
)

const (
	contentTypeJson = "application/json"
)

type handler struct {
	sbServer *seabattle.Server
	method   string
	cmd      uint8
}

func (h *handler) checkMethod(r *http.Request) bool {
	return r.Method == h.method
}

func (h *handler) sendError(w http.ResponseWriter, httpError int, err error) {
	w.WriteHeader(httpError)
	w.Write([]byte(err.Error()))
}

func (h *handler) getBody(w http.ResponseWriter, r *http.Request) ([]byte, error) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		h.sendError(w, http.StatusBadRequest, err)
	}
	defer r.Body.Close()

	return body, err
}

func (h *handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if !h.checkMethod(r) {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	data, err := h.getBody(w, r)
	if err != nil {
		return
	}

	c := make(chan *seabattle.Response)

	h.sbServer.ChanReq <- &seabattle.Request{
		Cmd:      h.cmd,
		Data:     data,
		ChanResp: c,
	}

	resp := <-c

	if resp.Error != nil {
		h.sendError(w, http.StatusBadRequest, resp.Error)
		return
	}

	w.Header().Set("Content-Type", contentTypeJson)
	w.Write(resp.Data)
}

func NewHandler(sb *seabattle.Server, method string, cmd uint8) *handler {
	return &handler{
		sbServer: sb,
		method:   method,
		cmd:      cmd,
	}
}
