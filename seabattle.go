package seabattle

import (
	"strings"
)

const maxDim = 26

type coord struct {
	row, col int
}

func (c *coord) lg(c2 *coord) bool {
	if c.col >= c2.col && c.row >= c2.row {
		return true
	}
	return false
}

type SeaBattle struct {
	dim     int                  // размерность
	ships   []map[coord]struct{} // координата, индекс -> id корабля
	sea     *sea
	shots   map[coord]struct{}
	knocked map[int]struct{}
	end     bool
}

func NewSeaBattle() *SeaBattle {
	return &SeaBattle{
		ships:   make([]map[coord]struct{}, 0),
		shots:   make(map[coord]struct{}),
		knocked: make(map[int]struct{}),
	}
}

func (sb *SeaBattle) countShips() int {
	return len(sb.ships)
}

func (sb *SeaBattle) clear() {
	if len(sb.ships) > 0 {
		sb.ships = sb.ships[0:0]
	}
	sb.sea = newSea(sb.dim)
	for c, _ := range sb.shots {
		delete(sb.shots, c)
	}
	for k, _ := range sb.knocked {
		delete(sb.knocked, k)
	}
	sb.end = false
}

func (sb *SeaBattle) createMatrix(rng int) {
	switch {
	case rng == 0:
		rng = 1
	case rng > maxDim:
		rng = maxDim
	}
	sb.dim = rng
	sb.clear()
}

func (sb *SeaBattle) createShip(ccStr string) error {
	if sb.end {
		sb.clear()
	}

	if sb.countShips() != 0 {
		return ErrorSeaFull
	}

	for _, cStr := range strings.Split(ccStr, ",") {
		if cc, err := getCoords(cStr); err != nil {
			sb.clear()
			return err
		} else {
			if cc[0].col > sb.dim || cc[0].row > sb.dim || cc[1].col > sb.dim || cc[1].row > sb.dim {
				sb.clear()
				return ErrorShipOutside
			}
			_, shipM, err := sb.sea.addShip([2]*coord{cc[0], cc[1]})
			if err != nil {
				sb.clear()
				return err
			}
			sb.ships = append(sb.ships, shipM)
		}
	}
	return nil
}

type StatShot struct {
	Destroy bool `json:"destroy"`
	Knock   bool `json:"knock"`
	End     bool `json:"end"`
}

func (sb *SeaBattle) shot(cStr string) (stat StatShot, err error) {
	var c *coord

	if sb.end {
		err = ErrorGameOver
		return
	}

	c, err = getCoord(cStr)
	if err != nil {
		return
	}

	if c.col > sb.dim || c.row > sb.dim {
		err = ErrorShotOutside
		return
	}

	if _, ok := sb.shots[*c]; ok {
		err = ErrorShotDouble
		return
	}
	sb.shots[*c] = struct{}{}

	stat.End = true
	for i, cc := range sb.ships {
		if _, ok := cc[*c]; ok {
			sb.knocked[i] = struct{}{}
			stat.Knock = true
			delete(cc, *c)
			if len(cc) == 0 {
				stat.Destroy = true
				delete(sb.knocked, i)
				if !stat.End {
					break
				}
			}
		}
		if stat.End && len(cc) > 0 {
			stat.End = false
		}
	}

	sb.end = stat.End

	return
}

type GameState struct {
	ShipCount int `json:"ship_count"`
	Destroyed int `json:"destroyed"`
	Knocked   int `json:"knocked"`
	ShotCount int `json:"shot_count"`
}

func (sb *SeaBattle) state() GameState {
	var d int

	for _, cc := range sb.ships {
		if len(cc) == 0 {
			d++
		}
	}

	return GameState{
		ShipCount: len(sb.ships),
		Destroyed: d,
		Knocked:   len(sb.knocked),
		ShotCount: len(sb.shots),
	}
}
