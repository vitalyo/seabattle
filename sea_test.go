package seabattle

import "testing"

func TestCreateShip(t *testing.T) {
	cc1 := [2]*coord{
		&coord{
			row: 2,
			col: 2,
		},
		&coord{
			row: 4,
			col: 3,
		},
	}

	dataB := [5]uint8{
		0,
		0b110,
		0b110,
		0b110,
		0,
	}

	dataM := map[coord]bool{
		coord{2, 2}: true,
		coord{3, 2}: true,
		coord{4, 2}: true,
		coord{2, 3}: true,
		coord{3, 3}: true,
		coord{4, 3}: true,
	}

	shipB, shipM := createShipOnSee(5, cc1)
	for i, row := range shipB {
		if dataB[i] != row {
			t.Errorf("%d: %b != %b", i, dataB[i], row)
		}
	}

	if len(shipM) != len(dataM) {
		t.Errorf("%d != %d", len(shipM), len(dataM))
	}
	for unit, _ := range shipM {
		if _, ok := dataM[unit]; !ok {
			t.Errorf("haven't %v", unit)
		}
	}

	s := newSea(5)

	if !s.checkFree(shipB) {
		t.Error("haven't place")
	}

	_, _, err := s.addShip(cc1)
	if err != nil {
		t.Error(err)
	}

	if s.checkFree(shipB) {
		t.Error("bad place")
	}

	cc2 := [2]*coord{
		&coord{
			row: 1,
			col: 2,
		},
		&coord{
			row: 3,
			col: 4,
		},
	}

	_, _, err = s.addShip(cc2)
	if err == nil {
		t.Error("bad place")
	}

	cc3 := [2]*coord{
		&coord{
			row: 3,
			col: 4,
		},
		&coord{
			row: 4,
			col: 5,
		},
	}

	_, _, err = s.addShip(cc3)
	if err != nil {
		t.Error(err)
	}

}
