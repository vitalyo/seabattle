package main

import (
	"log"
	"net/http"

	"local/seabattle"
	sbhttp "local/seabattle/http"
)

func main() {
	sb := seabattle.NewServer()

	http.Handle("/create-matrix", sbhttp.NewHandler(sb, http.MethodPost, seabattle.CmdCreateMatrix))
	http.Handle("/ship", sbhttp.NewHandler(sb, http.MethodPost, seabattle.CmdShip))
	http.Handle("/shot", sbhttp.NewHandler(sb, http.MethodPost, seabattle.CmdShot))
	http.Handle("/clear", sbhttp.NewHandler(sb, http.MethodPost, seabattle.CmdClear))
	http.Handle("/state", sbhttp.NewHandler(sb, http.MethodGet, seabattle.CmdState))

	log.Fatal(http.ListenAndServe(":8080", nil))
}
