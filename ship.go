package seabattle

func createShipOnSee(dim int, cc [2]*coord) (shipB []uint8, shipM map[coord]struct{}) {
	shipB = make([]uint8, dim, dim)
	shipM = make(map[coord]struct{})

	for row := 0; row < dim; row++ {
		for col := 0; col < dim; col++ {
			if col >= cc[0].col-1 && col <= cc[1].col-1 && row >= cc[0].row-1 && row <= cc[1].row-1 {
				shipB[row] = shipB[row] | (1 << col)
				shipM[coord{row + 1, col + 1}] = struct{}{}
			}
		}
	}
	return
}
