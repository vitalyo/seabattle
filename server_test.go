package seabattle

import (
	"encoding/json"
	"testing"
)

func TestServer(t *testing.T) {
	s := NewServer()

	c := make(chan *Response)

	// CmdCreateMatrix

	data, err := json.Marshal(&RequestCreateMatrix{
		Range: 5,
	})

	if err != nil {
		t.Fatal(err)
	}

	s.ChanReq <- &Request{
		Cmd:      CmdCreateMatrix,
		Data:     data,
		ChanResp: c,
	}

	resp := <-c

	if resp.Error != nil {
		t.Error(resp.Error)
	}

	// CmdShip

	data, err = json.Marshal(&RequestShip{
		Coordinates: "2B 4C, 3D 5E",
	})

	if err != nil {
		t.Fatal(err)
	}

	s.ChanReq <- &Request{
		Cmd:      CmdShip,
		Data:     data,
		ChanResp: c,
	}

	resp = <-c

	if resp.Error != nil {
		t.Error(resp.Error)
	}

	// CmdShot

	data, err = json.Marshal(&RequestShot{
		Coord: " 3C ",
	})

	if err != nil {
		t.Fatal(err)
	}

	s.ChanReq <- &Request{
		Cmd:      CmdShot,
		Data:     data,
		ChanResp: c,
	}

	resp = <-c

	if resp.Error != nil {
		t.Error(resp.Error)
	}

	statShot := StatShot{}
	err = json.Unmarshal(resp.Data, &statShot)
	if err != nil {
		t.Fatal(err)
	}

	dataShot := StatShot{
		Knock: true,
	}
	if statShot != dataShot {
		t.Errorf("%+v != %+v", statShot, dataShot)
	}

	// CmdState

	s.ChanReq <- &Request{
		Cmd:      CmdState,
		ChanResp: c,
	}

	resp = <-c

	if resp.Error != nil {
		t.Error(resp.Error)
	}

	gameState := GameState{}
	err = json.Unmarshal(resp.Data, &gameState)
	if err != nil {
		t.Fatal(err)
	}

	dataState := GameState{
		ShipCount: 2,
		Knocked:   1,
		ShotCount: 1,
	}
	if gameState != dataState {
		t.Errorf("%+v != %+v", gameState, dataState)
	}

	// CmdClear

	s.ChanReq <- &Request{
		Cmd:      CmdClear,
		ChanResp: c,
	}

	<-c

	// CmdState

	s.ChanReq <- &Request{
		Cmd:      CmdState,
		ChanResp: c,
	}

	resp = <-c

	if resp.Error != nil {
		t.Error(resp.Error)
	}

	gameState = GameState{}
	err = json.Unmarshal(resp.Data, &gameState)
	if err != nil {
		t.Fatal(err)
	}

	dataState = GameState{}
	if gameState != dataState {
		t.Errorf("%+v != %+v", gameState, dataState)
	}

}
