package seabattle

import "testing"

func TestCoord(t *testing.T) {
	a1 := &coord{
		row: 1,
		col: 1,
	}

	data := []struct {
		c   string
		res *coord
		err error
	}{
		{"1A", a1, nil},
		{" 1A  ", a1, nil},
		{" 0A  ", nil, ErrorBadCoord},
		{" 1AB  ", nil, ErrorBadCoord},
		{" A1  ", nil, ErrorBadCoord},
		{"11", nil, ErrorBadCoord},
		{" 12D  ", &coord{row: 12, col: 4}, nil},
	}

	for i, d := range data {
		c, err := getCoord(d.c)
		if err != d.err {
			t.Fatalf("bad coord, error: %v, iter = %d (%v)", err, i, c)
		}
		if err != nil {
			continue
		}
		if c.row != d.res.row {
			t.Errorf("bad row (%d != %d), iter = %d", c.row, d.res.row, i)
		}
	}
}

func TestCoords(t *testing.T) {
	cc := [2]*coord{
		&coord{
			row: 1,
			col: 1,
		},
		&coord{
			row: 2,
			col: 2,
		},
	}

	cc2 := [2]*coord{
		&coord{
			row: 1,
			col: 1,
		},
		&coord{
			row: 2,
			col: 1,
		},
	}

	cc3 := [2]*coord{
		&coord{
			row: 1,
			col: 1,
		},
		&coord{
			row: 1,
			col: 1,
		},
	}

	cc4 := [2]*coord{
		&coord{
			row: 2,
			col: 2,
		},
		&coord{
			row: 1,
			col: 1,
		},
	}

	data := []struct {
		c   string
		res [2]*coord
		err error
	}{
		{"1A  2B", cc, nil},
		{" 1A   2B     ", cc, nil},
		{" 1A   2A     ", cc2, nil},
		{" 1A   1A     ", cc3, nil},
		{" 2B  1A", cc4, ErrorBadCoord},
		{" 1A  ", cc, ErrorBadCoord},
	}

	for i, d := range data {
		cc, err := getCoords(d.c)
		if err != d.err {
			t.Fatalf("bad coord, error: %v, iter = %d (%v, %v)", err, i, cc[0], cc[1])
		}
		if err != nil {
			continue
		}
		if cc[0].row != d.res[0].row || cc[0].col != d.res[0].col {
			t.Fatalf("bad first (%d != %d), iter = %d", cc[0], d.res[0], i)
		}
		if cc[1].row != d.res[1].row || cc[1].col != d.res[1].col {
			t.Fatalf("bad second (%d != %d), iter = %d", cc[1], d.res[1], i)
		}
	}
}
