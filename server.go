package seabattle

import (
	"encoding/json"
	"log"
)

const (
	CmdCreateMatrix uint8 = iota
	CmdShip
	CmdShot
	CmdClear
	CmdState
)

type Request struct {
	Cmd      uint8
	Data     []byte
	ChanResp chan<- *Response
}

type Response struct {
	Data  []byte
	Error error
}

type Server struct {
	ChanReq   chan *Request
	seaBattle *SeaBattle
}

type RequestCreateMatrix struct {
	Range int `json:"range"`
}

type RequestShip struct {
	Coordinates string `json:"Coordinates"`
}

type RequestShot struct {
	Coord string `json:"coord"`
}

func NewServer() *Server {
	s := &Server{
		ChanReq:   make(chan *Request),
		seaBattle: NewSeaBattle(),
	}

	go s.run()

	return s
}

func (s *Server) run() {
	for req := range s.ChanReq {
		var err error
		switch req.Cmd {
		case CmdCreateMatrix:
			rq := new(RequestCreateMatrix)
			err = json.Unmarshal(req.Data, rq)

			switch {
			case err != nil:
				log.Printf("CmdCreateMatrix: error Unmarshal -> %v\n", err)
				req.ChanResp <- &Response{
					Error: err,
				}
			case rq.Range == 0:
				req.ChanResp <- &Response{
					Error: ErrorBadFields,
				}
			default:
				s.seaBattle.createMatrix(rq.Range)
				req.ChanResp <- new(Response)
			}

		case CmdShip:
			rq := new(RequestShip)
			err = json.Unmarshal(req.Data, rq)
			switch {
			case err != nil:
				log.Printf("CmdShip: error Unmarshal -> %v\n", err)
				req.ChanResp <- &Response{
					Error: err,
				}
			case rq.Coordinates == "":
				req.ChanResp <- &Response{
					Error: ErrorBadFields,
				}
			default:
				err = s.seaBattle.createShip(rq.Coordinates)
				req.ChanResp <- &Response{
					Error: err,
				}
			}

		case CmdShot:
			rq := new(RequestShot)
			err = json.Unmarshal(req.Data, rq)
			switch {
			case err != nil:
				log.Printf("CmdShot: error Unmarshal -> %v\n", err)
				req.ChanResp <- &Response{
					Error: err,
				}
			case rq.Coord == "":
				req.ChanResp <- &Response{
					Error: ErrorBadFields,
				}
			default:
				var (
					stat StatShot
					data []byte
				)

				stat, err = s.seaBattle.shot(rq.Coord)
				if err == nil {
					data, err = json.Marshal(stat)
				}
				req.ChanResp <- &Response{
					Error: err,
					Data:  data,
				}
			}

		case CmdClear:
			s.seaBattle.clear()
			req.ChanResp <- &Response{}

		case CmdState:
			var (
				stat GameState
				data []byte
			)

			stat = s.seaBattle.state()
			if err == nil {
				data, err = json.Marshal(stat)
			}
			req.ChanResp <- &Response{
				Error: err,
				Data:  data,
			}

		}
	}
}
