package seabattle

import "errors"

type sea struct {
	// поле представляется зеркально относительно вертикали,
	// столбец 'A' находиться справа
	rows []uint8
}

func newSea(dim int) *sea {
	return &sea{
		rows: make([]uint8, dim, dim),
	}
}

func (s *sea) checkFree(ship []uint8) bool {
	for i, row := range s.rows {
		if row&ship[i] != 0 {
			return false
		}
	}
	return true
}

func (s *sea) addShip(cc [2]*coord) (shipB []uint8, shipM map[coord]struct{}, err error) {
	shipB, shipM = createShipOnSee(len(s.rows), cc)
	if !s.checkFree(shipB) {
		err = errors.New("haven't place")
		return
	}
	for i, row := range s.rows {
		s.rows[i] = row | shipB[i]
	}
	return
}
