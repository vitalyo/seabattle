package seabattle

import "testing"

func TestSeaBattle(t *testing.T) {
	sb := NewSeaBattle()

	if sb.dim != 0 {
		t.Errorf("dim must be 0: %d", sb.dim)
	}
	lenShips := len(sb.ships)
	if lenShips != 0 {
		t.Errorf("len(ships) must be 0: %d", lenShips)
	}
	if sb.countShips() != 0 {
		t.Errorf("count ships must be 0: %d", sb.countShips())
	}

	dataCreate := []struct{ init, dim int }{
		{10, 10},
		{0, 1},
		{32, 26},
	}

	for _, d := range dataCreate {
		sb.createMatrix(d.init)
		if sb.dim != d.dim {
			t.Errorf("dim must be %d: %d", d.dim, sb.dim)
		}
		lenShips = len(sb.ships)
		if lenShips != 0 {
			t.Errorf("len(ships) must be 0: %d", lenShips)
		}
		if sb.countShips() != 0 {
			t.Errorf("count ships must be 0: %d", sb.countShips())
		}
	}

	dim := 5
	sb.createMatrix(dim)

	err := sb.createShip("2B 4C,,")
	if err == nil {
		t.Error("must be error")
	}

	err = sb.createShip("2B 4C,")
	if err == nil {
		t.Error("must be error")
	}

	err = sb.createShip("2B 4C, 3D 4F")
	if err == nil {
		t.Error("must be error")
	}

	err = sb.createShip("2B 4C, 3D 6E")
	if err == nil {
		t.Error("must be error")
	}

	err = sb.createShip("2B 4C, 3D 5E")
	if err != nil {
		t.Error(err)
	}

	if sb.countShips() != 2 {
		t.Errorf("count ships must be 2: %d", sb.countShips())
	}

	err = sb.createShip("2B 4C, 3D 5E")
	if err != ErrorSeaFull {
		t.Error(err)
	}
	if len(sb.ships) != 2 {
		t.Errorf("ships must be 2: %d", len(sb.ships))
	}

	dataShips := []map[coord]bool{
		map[coord]bool{
			coord{2, 2}: true,
			coord{2, 3}: true,
			coord{3, 2}: true,
			coord{3, 3}: true,
			coord{4, 2}: true,
			coord{4, 3}: true,
		},
		map[coord]bool{
			coord{3, 4}: true,
			coord{3, 5}: true,
			coord{4, 4}: true,
			coord{4, 5}: true,
			coord{5, 4}: true,
			coord{5, 5}: true,
		},
	}

	for i, s := range sb.ships {
		for unit, _ := range s {
			if _, ok := dataShips[i][unit]; !ok {
				t.Errorf("haven't %v", unit)
			}
		}
	}

	dataShot := []struct {
		name  string
		coord string
		stat  StatShot
		err   error
	}{
		{
			name:  "Out",
			coord: "4T",
			err:   ErrorShotOutside,
		},
		{
			name:  "Blank",
			coord: " 1A",
			err:   nil,
		},
		{
			name:  "Double",
			coord: " 1A",
			err:   ErrorShotDouble,
		},
		{
			name:  "Knock",
			coord: " 3C ",
			stat: StatShot{
				Knock: true,
			},
		},
		{
			name:  "Knock 2B",
			coord: "2B",
			stat: StatShot{
				Knock: true,
			},
		},
		{
			name:  "Knock 2C",
			coord: "2C",
			stat: StatShot{
				Knock: true,
			},
		},
		{
			name:  "Knock 3B",
			coord: "3B",
			stat: StatShot{
				Knock: true,
			},
		},
		{
			name:  "Double 3C",
			coord: "3C",
			err:   ErrorShotDouble,
		},
		{
			name:  "Knock 4B",
			coord: "4B",
			stat: StatShot{
				Knock: true,
			},
		},
		{
			name:  "Knock 4C",
			coord: "4C",
			stat: StatShot{
				Knock:   true,
				Destroy: true,
			},
		},
		{
			name:  "Blank 2A",
			coord: "2A",
		},
		{
			name:  "Knock 3D",
			coord: "3D",
			stat: StatShot{
				Knock: true,
			},
		},
		{
			name:  "Knock 4D",
			coord: "4D",
			stat: StatShot{
				Knock: true,
			},
		},
		{
			name:  "Knock 5D",
			coord: "5D",
			stat: StatShot{
				Knock: true,
			},
		},
		{
			name:  "Knock 3E",
			coord: "3E",
			stat: StatShot{
				Knock: true,
			},
		},
		{
			name:  "Knock 4E",
			coord: "4E",
			stat: StatShot{
				Knock: true,
			},
		},
		{
			name:  "Knock 5E",
			coord: "5E",
			stat: StatShot{
				Knock:   true,
				Destroy: true,
				End:     true,
			},
		},
		{
			name:  "Doble Knock 5E",
			coord: "5E",
			err:   ErrorGameOver,
		},
	}

	dataState := map[int]GameState{
		2: GameState{
			ShipCount: 2,
			ShotCount: 1,
		},
		8: GameState{
			ShipCount: 2,
			Knocked:   1,
			ShotCount: 6,
		},
		9: GameState{
			ShipCount: 2,
			Destroyed: 1,
			Knocked:   0,
			ShotCount: 7,
		},
		11: GameState{
			ShipCount: 2,
			Destroyed: 1,
			Knocked:   1,
			ShotCount: 9,
		},
		16: GameState{
			ShipCount: 2,
			Destroyed: 2,
			Knocked:   0,
			ShotCount: 14,
		},
		17: GameState{
			ShipCount: 2,
			Destroyed: 2,
			Knocked:   0,
			ShotCount: 14,
		},
	}

	for i, d := range dataShot {
		t.Run(d.name, func(t *testing.T) {
			if st, err := sb.shot(d.coord); err != d.err {
				t.Errorf("%v != %v", err, d.err)
			} else {
				if st != d.stat {
					t.Errorf("%v != %v", st, d.stat)
				}
			}
		})

		if ds, ok := dataState[i]; ok {
			if st := sb.state(); st != ds {
				t.Errorf("%v != %v", st, ds)
			}
		}
	}

	err = sb.createShip("2A 4C, 3D 5E")
	if err != nil {
		t.Errorf("error must be empty: %v", err)
	}

	sb.clear()

	gs := GameState{}
	if st := sb.state(); st != gs {
		t.Errorf("state must be empty: %+v", st)
	}

	if sb.dim != dim {
		t.Errorf("dim must be %d: %d", dim, sb.dim)
	}
	if sb.end {
		t.Error("end must be false")
	}

}
