package seabattle

import (
	"errors"
	"regexp"
	"strconv"
	"strings"
)

var (
	reCoord          = regexp.MustCompile(`^\s*(\d{1,2})([A-Z])\s*$`)
	ErrorBadCoord    = errors.New("Bad coordinate")
	ErrorShipOutside = errors.New("Ship outside the sea")
	ErrorSeaFull     = errors.New("Sea is full")
	ErrorShotOutside = errors.New("Shot outside the sea")
	ErrorShotDouble  = errors.New("Double shot")
	ErrorGameOver    = errors.New("Game over")
	ErrorBadFields   = errors.New("Bad fields")
)

func getCoord(c string) (*coord, error) {
	res := reCoord.FindStringSubmatch(c)
	if len(res) != 3 || len(res[1]) == 0 || len(res[2]) == 0 {
		return nil, ErrorBadCoord
	}

	crd := new(coord)
	var err error

	crd.row, err = strconv.Atoi(res[1])
	if err != nil || crd.row == 0 {
		return nil, ErrorBadCoord
	}

	crd.col = int(res[2][0] - 0x40)

	return crd, nil
}

func getCoords(cc string) (res []*coord, err error) {
	res = make([]*coord, 2, 2)
	var count int

	for _, c := range strings.Split(cc, " ") {
		if c != "" {
			if count > 2 {
				err = ErrorBadCoord
				return
			}
			res[count], err = getCoord(c)
			if err != nil {
				return
			}
			count++
		}
	}

	if res[0] == nil || res[1] == nil || !res[1].lg(res[0]) {
		err = ErrorBadCoord
		return
	}

	return
}
